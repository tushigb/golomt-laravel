<!doctype html>
<html lang="{{ app()->getLocale() }}" xmlns:v-on="http://www.w3.org/1999/xhtml"
      xmlns:v-bind="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700&amp;subset=cyrillic,cyrillic-ext"
          rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/golomt.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('css/uikit.css')}}" type="text/css"/>
    <link href="https://unpkg.com/ionicons@4.1.0/dist/css/ionicons.min.css" rel="stylesheet">
    <style>
        .mapouter {
            overflow: hidden;
            height: 450px;
            width: 100%;
        }

        .gmap_canvas {
            background: none !important;
            height: 450px;
            width: 100%;
        }
    </style>
    <title>BLOG | TUSHIG</title>
</head>
<body>
<div id="app">
    <div class="tm-background uk-background-cover" style="background-image: url('{{asset('images/background.jpg')}}')">
        <div class="uk-container">
            <div class="sc-header uk-flex uk-flex-middle uk-flex-column">
                <img src="{{asset('images/t_logo.png')}}" width="60px">
                <nav class="uk-navbar">
                    <div>
                        <ul class="uk-navbar-nav">
                            <li class="uk-active">
                                <a href="#modal-about" uk-toggle class="sc-text-200 sc-link-brand">Миний тухай</a></li>
                            <li class="uk-active">
                                <a class="sc-text-200 sc-link-brand">Холбогдох</a></li>
                        </ul>
                    </div>
                </nav>
                <nav class="uk-navbar">
                    <div>
                        <ul class="uk-navbar-nav">
                            <li v-bind:class="{'uk-active': 'all' == selected_category.name}">
                                <a v-on:click="selectCategory('all', 'all')"
                                   class="sc-text-200 sc-link-brand">
                                    Бүгд
                                </a>
                            </li>
                            <li v-for="(category, index) in categories"
                                v-bind:class="{'uk-active': category.name == selected_category.name}">
                                <a v-on:click="selectCategory(category.id, category.name)"
                                   class="sc-text-200 sc-link-brand">@{{
                                    category.name }}
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>

            <div class="uk-align-center uk-width-3-4">
                <div v-for="(content, index) in contents" class="uk-card uk-card-default uk-margin-medium-top"
                     style="border-radius: 25px; border-bottom-left-radius: 0;">
                    <div class="uk-card-media-top">
                        <img :src="'/storage/' + content.cover" alt="">
                    </div>
                    <div class="uk-card-body uk-padding-large">
                        <p class="uk-margin-remove-bottom uk-text-uppercase sc-text-400 uk-text-danger uk-visible@s">
                            @{{ content.category }}
                        </p>
                        <p class="uk-margin-remove-bottom uk-text-uppercase sc-text-size-11 sc-text-400 uk-text-danger uk-hidden@s">
                            @{{ content.category }}
                        </p>
                        <h1 class="uk-margin-remove-top uk-text-uppercase sc-text-400 sc-text-default uk-visible@s">
                            @{{ content.title }}
                        </h1>
                        <h4 class="uk-margin-remove-top uk-text-uppercase sc-text-400 sc-text-default uk-hidden@s">
                            @{{ content.title }}
                        </h4>
                        <p class="uk-article-meta sc-text-200">
                            Нийтэлсэн @{{ content.user.name }} |
                            <time datetime="2016-01-12T13:27:00+00:00">
                                @{{ content.created_at.date.substring(0, 10) }}
                            </time>
                        </p>
                        <p class="sc-text-200 sc-text-default">
                            @{{ content.description }}
                        </p>
                        <a v-on:click="getCurrentContent(content)" uk-toggle="target: #content-detail"
                           class="sc-text-200 sc-link-underline uk-margin-right">
                            Дэлгэрэнгүй
                        </a>
                        <a class="sc-text-200 sc-link-underline">
                            @{{ content.comments.length }} сэтгэгдэл
                        </a>
                    </div>
                </div>

                <div v-if="pagination != null">
                    <a v-if="pagination.links.next" v-on:click="getContents(pagination.links.next)">
                        <div class="uk-text-center uk-margin-top uk-align-right">
                            <div style="border-radius: 25px; border-bottom-left-radius: 0;"
                                 class="uk-card uk-card-default uk-card-hover uk-card-body ">
                                <h5 class="sc-text-200 sc-text-default">
                                    <b>Цааш унших <span class="icon ion-md-arrow-round-down"></span></b>
                                </h5>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
        </div>
        <div style="display: block;" id="spinner" class="uk-text-center uk-margin-large-top">
            <div style="color: #333F48" uk-spinner="ratio: 1.5"></div>
        </div>
    </div>
    <div class="uk-container uk-section uk-section-small">
        <div uk-grid>
            <div class="uk-navbar-left">
                <p class="sc-text-200 sc-text-default">
                    BLOG | TUSHIG
                </p>
            </div>
            <div class="uk-navbar-right">
                <a href="https://www.facebook.com/tushighen" target="_blank"
                   class="sc-text-size-18 sc-text-200 sc-link-default uk-margin-right">
                    <span class="icon ion-logo-facebook"></span>
                </a>
                <span class="uk-margin-right sc-text-default">|</span>
                <a href="https://www.instagram.com/tushgb/" target="_blank"
                   class="uk-margin-right sc-text-size-18 sc-text-200 sc-link-default">
                    <span class="icon ion-logo-instagram"></span>
                </a>
                <span class="uk-margin-right sc-text-default">|</span>
                <a href="https://github.com/tushighen" target="_blank"
                   class="uk-margin-right sc-text-size-18 sc-text-200 sc-link-default">
                    <span class="icon ion-logo-github"></span>
                </a>
                <span class="uk-margin-right sc-text-default">|</span>
                <a href="https://bitbucket.org/tushigb/" target="_blank"
                   class="uk-margin-right sc-text-size-18 sc-text-200 sc-link-default">
                    <span class="icon ion-logo-bitbucket"></span>
                </a>
            </div>
        </div>
    </div>
    @include('.layout.content_modal')
    @include('.layout.about_modal')
</div>
</body>
<script src="{{asset('js/uikit.min.js')}}"></script>
<script src="{{asset('js/uikit-icons.min.js')}}"></script>
<script src="{{asset('js/vuejs.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.3.6"></script>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            contents: [],
            pagination: null,
            current_content: {
                user: {},
                comments: []
            },
            comment: {},
            categories: [],
            selected_category: {
                id: null,
                name: null
            },
        },
        created: function () {
            this.getCategories();
        },
        methods: {
            getContents: function (url) {
                document.getElementById('spinner').style.display = 'block';
                this.$http.get(url).then(function (data) {
                    console.log(data.body.data);
                    for (var _i = 0; _i < data.body.data.length; _i++) {
                        this.contents.push(data.body.data[_i]);
                    }
                    this.pagination = data.body.meta.pagination;
                    document.getElementById('spinner').style.display = 'none';
                    console.log(this.pagination);
                })
            },
            getCurrentContent: function (content) {
                this.current_content = content;
                this.current_content.date = content.created_at.date.substring(0, 10);
            },
            getCategories: function () {
                this.$http.get('/rest/categories').then(function (data) {
                    this.categories = data.body;
                    this.selected_category.name = 'all';
                    this.getContents('/rest/contents/' + this.selected_category.name);
                })
            },
            selectCategory: function (id, category) {
                this.contents = [];
                this.pagination = {};
                if (id == 'all') {
                    this.selected_category.name = 'all';
                    this.getContents('rest/contents/all');
                } else {
                    this.selected_category.id = id;
                    this.selected_category.name = category;
                    this.getContents('/rest/contents/' + this.selected_category.id);
                }
            },
            addComment: function () {
                var jsonObject = JSON.stringify(this.comment);
                this.$http.post('/rest/comment/' + this.current_content.id, jsonObject).then(function (data) {
                    this.comment.gender = parseInt(this.comment.gender);
                    this.current_content.comments.push(this.comment);
                    this.comment = {};
                })
            }
        }
    });
</script>
</html>