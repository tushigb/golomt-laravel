<div v-if="current_content" id="content-detail" class="uk-modal-container sc-content-modal" uk-modal
     xmlns:v-bind="http://www.w3.org/1999/xhtml" xmlns:v-on="http://www.w3.org/1999/xhtml">
    <div class="uk-modal-dialog">
        <div>
            <div style="border-style: none none solid none; border-width: 1px; border-color: #f3f4f7;"
                 class="uk-grid-collapse uk-child-width-1-1@s uk-flex-middle uk-grid" uk-grid>
                <div uk-scrollspy="cls: uk-animation-fade;" class="uk-background-cover uk-first-column"
                     v-bind:style="{'background-image': 'url(' + '/storage/' +  current_content.cover + ')'}"
                     style="box-sizing: border-box;
                             min-height: 50vh; height: 50vh;"
                ></div>
            </div>
        </div>
        <div class="uk-modal-body uk-width-3-5@l uk-width-3-4@m uk-width-1-1@s uk-align-center">
            <h1 class="uk-container uk-text-uppercase sc-text-200 sc-text-default">@{{ current_content.title }}</h1>
            <p class="uk-container uk-article-meta sc-text-200">
                Нийтэлсэн @{{ current_content.user.name}} |
                <time datetime="2016-01-12T13:27:00+00:00">
                    @{{ current_content.date }}
                </time>
            </p>
            <p class="uk-container sc-text-200 sc-text-default">
                @{{ current_content.description }}
            </p>
            <div class="uk-container">
                <article class="uk-article" v-html="current_content.body">
                </article>
            </div>
        </div>
        <div class="uk-section uk-section-small uk-section-muted uk-text-center">
            <h2>
                <span class="icon ion-md-chatbubbles"></span>&nbsp;&nbsp;Сэтгэгдэл бичих
            </h2>
        </div>
        <div class="uk-modal-body uk-width-3-5@l uk-width-3-4@m uk-width-1-1@s uk-align-center">
            <form class="uk-width-2-3 uk-align-center" v-on:submit.prevent="addComment">
                <fieldset class="uk-fieldset">
                    <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
                        <label><input v-model="comment.gender" value="1" class="uk-radio" type="radio" name="radio2"
                                      checked> Эрэгтэй</label>
                        <label><input v-model="comment.gender" value="0" class="uk-radio" type="radio" name="radio2">
                            Эмэгтэй</label>
                    </div>
                    <div class="uk-margin">
                        <input v-model="comment.name" class="uk-input" type="text" placeholder="Таны нэр">
                    </div>
                    <div class="uk-margin">
                        <textarea v-model="comment.comment" class="uk-textarea" rows="4" style="resize: vertical"
                                  placeholder="Таны сэтгэгдэл"></textarea>
                    </div>
                    <div class="uk-margin">
                        <button style="border-radius: 5px" class="uk-width-1-1 uk-button uk-button-score">Сэтгэгдэл
                            бичих
                        </button>
                    </div>
                    <hr>
                </fieldset>
            </form>
            <p v-if="current_content.comments.length > 0" class="uk-container"><b>Сэтгэгдэл (@{{ current_content.comments.length }})</b></p>
            <article v-for="(comment, index) in current_content.comments"
                     class="uk-container uk-comment uk-margin-bottom">
                <header class="uk-comment-header uk-grid-medium uk-flex-middle" uk-grid>
                    <div class="uk-width-auto">
                        <img v-if="comment.gender" class="uk-comment-avatar" :src="'/images/male.png'" width="50"
                             height="50" alt="">
                        <img v-if="!comment.gender" class="uk-comment-avatar" :src="'/images/female.png'" width="50"
                             height="50" alt="">
                    </div>
                    <div class="uk-width-expand">
                        <h4 class="uk-comment-title uk-margin-remove"><a class="uk-link-reset" href="#">
                                @{{ comment.name }}
                            </a>
                        </h4>
                        <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
                            <li>@{{ comment.date }}</li>
                        </ul>
                    </div>
                </header>
                <div class="uk-comment-body">
                    <p>
                        @{{ comment.comment }}
                    </p>
                </div>
                <hr v-if="current_content.comments.length - 1 != index">
            </article>
            {{--<article class="uk-container uk-comment uk-margin-bottom">--}}
            {{--<header class="uk-comment-header uk-grid-medium uk-flex-middle" uk-grid>--}}
            {{--<div class="uk-width-auto">--}}
            {{--<img class="uk-comment-avatar" :src="'/images/female.png'" width="50" height="50" alt="">--}}
            {{--</div>--}}
            {{--<div class="uk-width-expand">--}}
            {{--<h4 class="uk-comment-title uk-margin-remove"><a class="uk-link-reset" href="#">Author</a>--}}
            {{--</h4>--}}
            {{--<ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">--}}
            {{--<li><a href="#">12 days ago</a></li>--}}
            {{--</ul>--}}
            {{--</div>--}}
            {{--</header>--}}
            {{--<div class="uk-comment-body">--}}
            {{--<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor--}}
            {{--invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam--}}
            {{--et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est--}}
            {{--Lorem ipsum dolor sit amet.</p>--}}
            {{--</div>--}}
            {{--</article>--}}
        </div>
    </div>
</div>