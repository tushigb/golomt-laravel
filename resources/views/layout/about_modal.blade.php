<div id="modal-about" class="uk-modal-container" uk-modal>
    <div class="uk-modal-dialog uk-modal-body uk-padding-large">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <div>
            <img class="uk-border-circle uk-align-center" src="{{asset('images/tushig.jpg')}}"
                 width="300" height="300">
            <h1 class="uk-text-center sc-text-200 sc-text-default">TUSHIG BATTUMUR</h1>
            <p class="uk-text-center sc-text-200 sc-text-default">DEVELOPER</p>

            <ul class="uk-breadcrumb uk-text-center">
                <li><p class="sc-text-200 uk-text-danger">Java</p></li>
                <li><p class="sc-text-200 sc-text-default">Spring Boot Framework</p></li>
                <li><p class="sc-text-200 uk-text-danger">JavaScript</p></li>
                <li><p class="sc-text-200 sc-text-default">Angular</p></li>
                <li><p class="sc-text-200 sc-text-default">VueJS</p></li>
                <li><p class="sc-text-200 sc-text-default">React Native</p></li>
                <li><p class="sc-text-200 sc-text-default">Meteor</p></li>
                <li><p class="sc-text-200 uk-text-danger">PHP</p></li>
                <li><p class="sc-text-200 sc-text-default">Laravel</p></li>
                <li><p class="sc-text-200 uk-text-danger">CSS</p></li>
                <li><p class="sc-text-200 sc-text-default">UIKit</p></li>
                <li><p class="sc-text-200 sc-text-default">Bootstrap</p></li>
                <li><p class="sc-text-200 uk-text-danger">MySQL</p></li>
                <li><p class="sc-text-200 uk-text-danger">SQLite</p></li>
            </ul>

            <div class="uk-grid-small" uk-grid>
                <div class="uk-width-expand sc-text-200 sc-text-default" uk-leader>
                    NOUS LLC / Дадлага
                </div>
                <div class="sc-text-200 sc-text-default">2015/11/15 - 2016/04/30</div>
            </div>
            <div class="uk-grid-small" uk-grid>
                <div class="uk-width-expand sc-text-200 sc-text-default" uk-leader>
                    ТекАвдар / Хөгжүүлэгч
                </div>
                <div class="sc-text-200 sc-text-default">2018/01/15 - 2018/05/31</div>
            </div>
            <div class="uk-grid-small" uk-grid>
                <div class="uk-width-expand sc-text-200 sc-text-default" uk-leader>
                    ШУТИС - МХТС / Баклавр
                </div>
                <div class="sc-text-200 sc-text-default">2018/06/04</div>
            </div>
            <div class="uk-grid-small" uk-grid>
                <div class="uk-width-expand sc-text-200 sc-text-default" uk-leader>
                    ГОЛОМТ БАНК ? / ?
                </div>
                <div class="sc-text-200 sc-text-default">?</div>
            </div>
            {{--<div class="uk-child-width-1-2 uk-child-width-1-3@s uk-grid-match uk-grid-small" uk-grid>--}}
            {{--<div class="uk-text-center">--}}
            {{--<div class="uk-inline-clip uk-transition-toggle" tabindex="0">--}}
            {{--<img class="uk-transition-scale-up uk-transition-opaque" src="{{asset('images/tushig.jpg')}}"--}}
            {{--alt="">--}}
            {{--</div>--}}
            {{--<p class="uk-margin-small-top">Scale Up Image</p>--}}
            {{--</div>--}}
            {{--<div class="uk-text-center">--}}
            {{--<div class="uk-inline-clip uk-transition-toggle" tabindex="0">--}}
            {{--<img class="uk-transition-scale-up uk-transition-opaque" src="{{asset('images/tushig.jpg')}}"--}}
            {{--alt="">--}}
            {{--</div>--}}
            {{--<p class="uk-margin-small-top">Scale Up Image</p>--}}
            {{--</div>--}}
            {{--<div class="uk-text-center">--}}
            {{--<div class="uk-inline-clip uk-transition-toggle" tabindex="0">--}}
            {{--<img class="uk-transition-scale-up uk-transition-opaque" src="{{asset('images/tushig.jpg')}}"--}}
            {{--alt="">--}}
            {{--</div>--}}
            {{--<p class="uk-margin-small-top">Scale Up Image</p>--}}
            {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
</div>