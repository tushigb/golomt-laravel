<?php namespace App\Transformer;

use App\Category;
use App\Comment;
use App\User;
use League\Fractal\TransformerAbstract;

class ContentTransformer extends TransformerAbstract
{
    public function __construct()
    {

    }

    public function transform($content)
    {
        return [
            'id' => $content->id,
            'title' => $content->title,
            'description' => $content->description,
            'cover' => $content->cover,
            'body' => $content->body,
            'created_at' => $content->created_at,
            'user' => User::find($content->user_id),
            'comments' => Comment::where('content_id', '=', $content->id)->get(),
            'category' => Category::find($content->category_id)['name'],
        ];
    }
}