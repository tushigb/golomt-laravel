<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $fillable = [
        'title', 'description', 'cover', 'body',
        'category_id', 'user_id', 'edited_user_id'
    ];
}