<?php

namespace App\Http\Controllers;

use App\Comment;
use Carbon\Carbon;
use EllipseSynergie\ApiResponse\Laravel\Response;
use Illuminate\Http\Request;

class CommentController extends Controller
{

    protected $response;
    protected $comment;

    public function __construct(Response $response, Comment $comment)
    {
        $this->response = $response;
        $this->comment = $comment;
    }

    public function save(Request $request, $id)
    {
        try {
            $inputs = $request->all();
            $inputs['content_id'] = $id;
            $inputs['date'] = Carbon::now();
            $comment = $this->comment->create($inputs);
            if ($comment) {
                return response()->json(['message' => 'successful'], 200);
            }
        } catch (\Exception $e) {
            return $this->response->errorInternalError($e);
        }
    }
}
