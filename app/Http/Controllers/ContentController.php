<?php

namespace App\Http\Controllers;

use App\Content;
use App\Image;
use App\Transformer\ContentTransformer;
use App\User;
use EllipseSynergie\ApiResponse\Laravel\Response;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class ContentController extends Controller
{
    protected $response;
    protected $content;
    protected $image;

    public function __construct(Response $response, Content $content, Image $image)
    {
        $this->response = $response;
        $this->content = $content;
        $this->image = $image;
    }

    // Infinity scroll
    public function getAll($category_id)
    {
        if ($category_id == 'all') {
            $contents = Content::orderBy('id', 'desc')
                ->paginate(1);
        } else {
            $contents = Content::where('category_id', '=', $category_id)
                ->orderBy('id', 'desc')
                ->paginate(1);
        }
        return $this->response->withPaginator($contents, new ContentTransformer());
    }

    public function getAllByUserId()
    {
        try {
            $contents = Content::where('user_id', '=', Auth::id())
                ->orderBy('id', 'desc')
                ->paginate(3);
            return $this->response->withPaginator($contents, new ContentTransformer());
        } catch (\Exception $e) {
            return $this->response->errorInternalError($e);
        }
    }

    public function getOne($id)
    {
        try {
            $content = Content::find($id);
            $content->images = [];
            return response()->json($content);
        } catch (\Exception $e) {
            return $this->response->errorInternalError($e);
        }
    }

    public function save(Request $request)
    {
        try {
            $inputs = $request->all();
            if (isset($inputs['id'])) {
                $inputs['edited_user_id'] = Auth::id();
                $content = Content::find($inputs['id']);
                $content->update($inputs);
                if ($request->has('images')) {
                    $images = $request->input('images');
                    foreach ($images as $key => $image) {
                        if ($key == 0) {
                            $content->cover = $image['path'];
                            $content->update();
                        } else {
                            $inputs['path'] = $image['path'];
                            $inputs['content_id'] = $content->id;
                            $this->image->create($inputs);
                        }
                    }
                }
                return response()->json(['message' => 'successful'], 200);
            } else {
                $inputs['user_id'] = Auth::id();
                $content = $this->content->create($inputs);
                if ($content) {
                    if ($request->has('images')) {
                        $images = $request->input('images');
                        foreach ($images as $key => $image) {
                            if ($key == 0) {
                                $content->cover = $image['path'];
                                $content->update();
                            } else {
                                $inputs['path'] = $image['path'];
                                $inputs['content_id'] = $content->id;
                                $this->image->create($inputs);
                            }
                        }
                    }
                    return response()->json(['message' => 'successful'], 200);
                } else return response()->json(['message' => 'error'], 200);
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function remove($id)
    {
        try {
            $content = Content::find($id);
            $content->delete();
            return response()->json(['message' => 'successful', 200]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}