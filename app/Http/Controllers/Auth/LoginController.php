<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('guest')->except('logout');
    }

    //Нэвтрэх
    public function login(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if ($user) {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                $user = User::where("email", $request->email)->first();
                return response()->json(['user' => $user], 200);
            } else {
                return response()->json(['responseMessage' => 'Имэйл эсвэл нууц үг буруу байна!'], 401);
            }
        } else {
            return response()->json(['responseMessage' => 'Бүртгэлгүй хэрэглэгч байна!'], 401);
        }
    }

    //Гарах
    public function logout()
    {
        Auth::logout();
        return response()->json(['responseMessage' => 'User logged out'], 200);
    }

    //Нэвтрэлт баталгаажуулалт
    public function verify()
    {
        if (Auth::check()) {
            $user = User::where('id', '=', Auth::id())
                ->first();
            return response()->json(['user' => $user,
                'responseMessage' => 'verified']);
        } else
            return response()->json(['responseMessage' => 'not verified'], 401);
    }
}
