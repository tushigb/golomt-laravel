<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;

class HelperController extends Controller
{
    // Зураг upload
    public function uploadImage(Request $request, $folder_name)
    {
        try {
            $validator = Validator::make($request->all(), [
                'image_file' => 'required|image|mimes:jpeg,png,jpg|max:3072'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 401);
            }

            $imageName = $request->file('image_file')->store($folder_name);

            if ($imageName)
                return response()->json(['responseMessage' => 'You have successfully uploaded image',
                    'imagePath' => $imageName], 200);
            else return response()->json(['responseMessage' => 'error'], 500);
        } catch (\Exception $e) {
            return $this->response->errorInternalError($e->getMessage());
        }
    }

    // Зураг устгах
    public function deleteImage(Request $request)
    {
        try {
            Storage::delete($request->image_url);
            return response()->json([
                'responseMessage' => 'You have successfully deleted image'], 200);
        } catch (\Exception $e) {
            return $this->response->errorInternalError($e->getMessage());
        }
    }

    // Category авах
    public function getCategories()
    {
        try {
            return response()->json(Category::all());
        } catch (\Exception $e) {
            return $this->response->errorInternalError($e->getMessage());
        }
    }
}
