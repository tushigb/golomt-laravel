<?php

Route::post('login', 'Auth\LoginController@login')->name('login');

Route::get('logout', 'Auth\LoginController@logout');

Route::get('verify', 'Auth\LoginController@verify');