<?php

Route::post('rest/content', 'ContentController@save');

Route::get('rest/contents/{id}', 'ContentController@getAll');

Route::get('rest/contents', 'ContentController@getAllByUserId');

Route::get('rest/content/{id}', 'ContentController@getOne');

Route::delete('rest/content/{id}', 'ContentController@remove');

Route::post('rest/comment/{id}', 'CommentController@save');