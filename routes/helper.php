<?php

//Зураг upload
Route::post('image/upload/{folder_name}', 'HelperController@uploadImage');

//Зураг устгах
Route::post('image/delete', 'HelperController@deleteImage');

//Category авах
Route::get('rest/categories', 'HelperController@getCategories');

Route::any('{path?}', function () {
    return File::get('dist/index.html');
})->where("path", ".+");