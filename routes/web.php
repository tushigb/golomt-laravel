<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $categories = \App\Category::all();
    $data = array(
        'categories' => $categories,
    );
    return view('index', $data);
});

Route::get('/storage/{folder}/{filename}', function ($folder, $filename) {
    if ($folder != 'file') {
        $path = storage_path('app/' . $folder . '/' . $filename);

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }
});
