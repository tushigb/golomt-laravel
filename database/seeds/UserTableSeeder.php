<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Tushig',
                'email' => 'tushig.0803@gmail.com',
                'password' => '$2y$10$EDXv/9dVdk6upcno9Gcd/.a6mFIWTeaj21CpMByvfEXqbxqfTHdpa',
                'role_id' => '1'
            ]
        ];

        foreach ($users as $user) {
            DB::table('users')->insert([
                'name' => $user['name'],
                'email' => $user['email'],
                'password' => $user['password'],
                'role_id' => $user['role_id']
            ]);
        }
    }
}
